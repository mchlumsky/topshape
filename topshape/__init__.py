# -*- coding: utf-8 -*-
"""
Modules:
 * topshape: main application module

Sub-packages:
 * tests: contains testing code
"""
from .topshape import *

__author__ = 'Martin Chlumsky'
__email__ = 'martin.chlumsky@gmail.com'
__version__ = '1.0.0'
