=======
History
=======

1.0.0 (2017-04-09)
------------------

* Remove classes EdgeText and KeyHandler.
* Add ability to filter rows by regex.
* Header function is now mandatory.
* Footer function is now optional.
* Rename argument key_mapping to key_map in TopShape.create_app()

0.2.0 (2017-02-20)
------------------

* Add functionality to get text input from user.

0.1.1 (2017-01-10)
------------------

* First release on PyPI.
